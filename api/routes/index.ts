import * as express from 'express';
const router = express.Router();
import { Config } from '../../config.global';

router.get('/', (req, res) => {
	res.send('Welcome on API. Environement: ' + Config.environementType());
});

export const index = router;