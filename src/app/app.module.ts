import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { TransferHttpCacheModule } from '@nguniversal/common';

const routes = [
	{ path: '', component: HomeComponent, pathMatch: 'full' },
	{ path: 'lazy', loadChildren: './components/lazy/lazy.module#LazyModule' },
	{ path: 'lazy/nested', loadChildren: './components/lazy/lazy.module#LazyModule' }
];

@NgModule({
	declarations: [
		AppComponent,
		HomeComponent
	],
	imports: [
		BrowserModule.withServerTransition({ appId: 'ng-universal-demo' }),
		RouterModule.forRoot(routes),
		TransferHttpCacheModule,
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
